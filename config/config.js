require('dotenv').config();

module.exports = {
    ACCESS_KEY_ID :process.env.ACCESS_KEY_ID,
    SECRET_ACCESS_KEY:process.env.SECRET_ACCESS_KEY,
    REGION:process.env.REGION,
    EMAIL:process.env.EMAIL,
    PWD:process.env.PWD
};