var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

module.exports = function (Schema,tableName) {
	//console.log("tableName: "+tableName);
	const Model = mongoose.model(tableName, Schema);

	//CRUD operations for Models
	const createItem = function(params, callback){
		Model.save(params, (err, createdData) => {
			if (err) console.log('\nItem creation err\n', err);
			else {
				console.log('\nCreated an item in the table\n', );
				callback(null, createdData);
			}
		});
	}
	
	const getItem = function(params,callback,limit){
		const getCallback = (err, result)=> {
			if (err) {
				console.log(`Get item err..\n\n`, err);
				callback(err, {});
			} else if (!result) {
				//console.log('result: ', result);
				console.log(`\nNo item found..`);
				callback(null, false);
			} else {
				console.log(`\nGet item success..`);
				//console.log('result: ', result);
				callback(null, result);
			}
		}

		//Model.find(params, getCallback);
		Model.find(params,getCallback).sort({_id:-1}).limit(limit);
	}
	
	const getUserById = function(params, callback){
		const getCallback = (err, result)=> {
			if (err) {
				console.log(`Get item err..\n\n`, err);
				callback(err, {});
			} else if (!result) {
				//console.log('result: ', result);
				console.log(`\nNo item found..`);
				callback(null, false);
			} else {
				console.log(`\nGet item success..`);
				//console.log('result: ', result);
				callback(null, result);
			}
		}

		Model.findById(params, getCallback);
	}
	
	const deleteItem = function(params, callback){
		Model.findByIdAndRemove(params,(err,data) => {
			if (err) console.log('err:\n\n', err);
			else {
				console.log(`Removed an object with ${params} from db`);
				//console.log('\ndeleted item',data.attrs)
				callback(null, data);
			}
		})
		
	}
	
	const getItematCat = function(params, callback){
		const getCallback = (err, result)=> {
			if (err) {
				console.log(`Get item err..\n\n`, err);
				callback(err, {});
			} else if (!result) {
				//console.log('result: ', result);
				console.log(`\nNo item found..`);
				callback(null, false);
			} else {
				//console.log(`\nGet item success..`);
				console.log('result: ', result);
				callback(null, result);
			}
		}

		Model.find(params, getCallback);
	}
	
	const updateItem = function (id,params, callback) {   
		Model.findByIdAndUpdate(id,{$set:res},(err, updatedData) => {
			if (err) console.log('\nItem Update error', err);
			else {
				console.log('Updated data');
				callback(null, updatedData)
			}
		});
	}

	const getItems = function(callback,limit){
		Model.find(callback).sort({_id:-1}).limit(limit);
			
	}
	
	// const getcatItems = function(callback,limit){
	// 	Model.find(callback).sort({_id:-1}).limit(limit);
			
	// }
	
	const getLatest = function(callback,limit){
		Model.find(callback).sort({_id:-1}).limit(3);
	}

	return {
		createItem,
		getItem,
		updateItem,
		deleteItem,
		getUserById,
		getItems,
		getLatest
	}

}
