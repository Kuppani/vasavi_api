var express = require('express');
var router = express.Router();
var passport = require('passport');
var path = require('path');
var multer = require('multer');
var LocalStrategy = require('passport-local').Strategy;
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var nodemailer = require("nodemailer");
const Mail = require('../methods/email');

var Award = require('../models/award');
var News = require('../models/news');
var Banner = require('../models/banner');
var Gallery = require('../models/exp_drives');
var Magazine = require('../models/magazine');
var Orientation = require('../models/orientation');
var Academic = require('../models/academic');
var Nonacademic = require('../models/non_academic');
var Parentedu = require('../models/parent_edu');
var Career = require('../models/career_guide');
var Book = require('../models/books');
var Qpaper = require('../models/question_paper');
var Supplements = require('../models/iit_supplements');
var Guest = require('../models/guest_talks');
var Admission = require('../models/admission');
var Application = require('../models/application');

//S3 bucket 
const config = require('../config/config');
var aws = require('aws-sdk');
var fs = require('fs');

/******************************************
****Image uploading into S3 code start*****
*******************************************/
	
 /* Multer set storage location*/
 var storage = multer.diskStorage({
	
	filename: function (req, file, cb) {
		   var str = file.originalname;
		   str = str.replace(/\s+/g, '-').toLowerCase();
		   global.poster = Date.now() + '_' + str;
		   cb(null, poster);
	 }
   });

   var upload = multer({ storage: storage });
   aws.config.update({ accessKeyId: config.ACCESS_KEY_ID, secretAccessKey: config.SECRET_ACCESS_KEY });
   aws.config.update({region: config.REGION});

/******************************************
****Image uploading into S3 code end*****
 *******************************************/

 //Nodemailer
var smtpTransport = nodemailer.createTransport({
    service: 'Gmail', // sets automatically host, port and connection security settings
    auth: {
        user: 'kuppanipravallika201@gmail.com',//config.EMAIL, 
        pass: 'kmmdpry1'//config.PWD  
    }
});

router.get('/home',function(req, res){
	Award.getItem({category:"home"},function(err,result){
		if(err) throw err;
		else{
		  res.send(result);
		}
	});
});

router.get('/latestnews',function(req, res){
	News.getLatest(function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/gallerylist',function(req, res){
	Gallery.getItems(function(err,result){
		if(err) throw err;
		else{
			//console.log("gallery::"+result);
			res.json(result);
		}
	});
});

router.get('/newslist',function(req, res){
	News.getItems(function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

// router.get('/banner/:cat',function(req, res){
// 	Banner.getItem({category:req.params.cat},function(err,result){
// 		if(err) throw err;
// 		else{
// 		  res.json(result);
// 		}
// 	});
// });

router.get('/banner/:cat',function(req, res){
	Banner.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		else{
		  res.json(result);
		}
	});
});

router.get('/magzinelist',function(req, res){
	Magazine.getItems(function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/orientationlist/:cat',function(req, res){
	Orientation.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/awardlist/:cat',function(req, res){
	Award.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});
//academiclist

router.get('/academiclist/:cat',function(req, res){
	Academic.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/nonacademiclist/:cat',function(req, res){
	Nonacademic.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/parentedulist/:cat',function(req, res){
	Parentedu.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/careerlist/:cat',function(req, res){
	Career.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/booklist/:cat',function(req, res){
	Book.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/qpaperlist/:cat',function(req, res){
	Qpaper.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/supplimentlist/:cat',function(req, res){
	Supplements.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.get('/guesttalkslist/:cat',function(req, res){
	Guest.getItem({category:req.params.cat},function(err,result){
		if(err) throw err;
		res.json(result);
	});
});

router.post('/admission',function(req, res){

	var bodyparams=req.body;
	//console.log(bodyparams);
	const params = {};
	var obj=Object.assign({},req.body);
	for (var key in obj) {
		if (obj.hasOwnProperty(key)){
		  params[key] = obj[key];
		}
	  }
	  console.log("target::"+JSON.stringify(params));
	params['created_on'] = Date.now();
	var postAdmission = new Admission(params);

	Admission.saveAdmission(postAdmission,function(err,result){
		if(err) throw err;
		else{
			//res.json(result);
			const mailOptions = Mail.sendMailOptions();
			smtpTransport.sendMail(mailOptions, (error, info) => {
				if (error) {
					return console.log('Error while sending mail: ' + error);
				} else {
					console.log('Message sent: %s', info.messageId);
					res.send('success');
				}
				smtpTransport.close(); // shut down the connection pool, no more messages.
			});
		}
	});
});

router.post('/application',upload.single('Cv'),function(req, res){

	var bodyparams=req.body;
	console.log(bodyparams);
	const params = {};
	var obj=Object.assign({},req.body);
	for (var key in obj) {
		if (obj.hasOwnProperty(key)){
		  params[key] = obj[key];
		}
	  }
	//console.log("target::"+JSON.stringify(params));
	params['created_on'] = Date.now();

	  	//console.log("image:"+req.file.originalname);
		var s3 = new aws.S3();
		var hello=req.file.path;
						s3.upload({
							"ACL":"public-read",
							"Bucket": "vasavibannerimages",
							"Key": poster,
							"Body": fs.createReadStream(hello),
							ContentType: "application/pdf",
							ContentDisposition: "inline"
							}, function(err, data) {
							if (data) {
								console.log("Image uploaded successfully");
								const imageurl="https://s3.ap-south-1.amazonaws.com/vasavibannerimages/"+poster;
								params['Cv'] = imageurl;
								//console.log("banner:::"+JSON.stringify(params));
								var postApplication = new Application(params);

								Application.saveApplication(postApplication,function(err,result){
									if(err) throw err;
									else{
										//res.json(result);

									}
								});
							}
						});
});

module.exports = router;