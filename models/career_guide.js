const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Career Schema
const tableName="Career";
const CareerSchema = mongoose.Schema({
	category: {
		type: String
	},
	title: {
		type: String
	},
	url: {
		type: String
   },
   image: {
		type: String
   },
   created_on: {
		type: String
   }
});

const Career = SchemaModel(CareerSchema, tableName);

module.exports = Career;