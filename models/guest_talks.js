const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Guest Schema
const tableName="Guesttalks";
var GuestSchema = mongoose.Schema({

category: {
	type: String
},
name: {
	type: String
},
expertise: {
	type: String
},
date: {
	type: String
},
description: {
	type: String
},
url: {
	type: String
},
file: {
	  type: String
},
created_on: {
		type: String
}
});

const Guest = SchemaModel(GuestSchema, tableName);

module.exports = Guest;
