const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Orientation Schema
const tableName="Orientation";
const OrientationSchema = mongoose.Schema({

orientation_on: {
	type: String
},
orientation_by: {
	type: String
},
description: {
	type: String
},
url: {
	type: String
},
file: {
	  type: String
},
created_on: {
		type: String
}
});

const Orientation = SchemaModel(OrientationSchema, tableName);

module.exports = Orientation;
