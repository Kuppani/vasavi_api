const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Banner Schema
const tableName="Banner";
const BannerSchema = mongoose.Schema({
	
	banner_image: {
		type: String
	},
	category: {
		type: String
    },
	created_on: {
			type: String
	},
	status: {
			type: String
	}
});

const Banner = SchemaModel(BannerSchema, tableName);

module.exports = Banner;