const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Book Schema
const tableName="Book";
const BookSchema = mongoose.Schema({
	
	category: {
		type: String
	},
	book_category: {
		type: String
    },
	title: {
			type: String
	},
	author: {
			type: String
	},
	file: {
			type: String
	},
	created_on: {
			type: String
	}
});

const Book = SchemaModel(BookSchema, tableName);

module.exports = Book;