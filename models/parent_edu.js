const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Parent Schema
const tableName="Parent";
var ParentSchema = mongoose.Schema({
	category: {
		type: String
	},
	title: {
		type: String
	},
	url: {
		type: String
	},
	description: {
		type: String
    },
    file: {
		type: String
   },
   created_on: {
		type: String
   }
});

const Parent = SchemaModel(ParentSchema, tableName);

module.exports = Parent;