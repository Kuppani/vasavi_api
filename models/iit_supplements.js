const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Supplements Schema
const tableName="Suplements";
const SupplementsSchema = mongoose.Schema({
	
	category: {
		type: String
	},
  class: {
		type: String
  },
  subject: {
    type: String
  },
  file: {
		type: String
	},
  created_on: {
		type: String
  }
});

const Supplements = SchemaModel(SupplementsSchema, tableName);

module.exports = Supplements;