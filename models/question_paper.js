const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Qpaper Schema
const tableName="QuestionPaper";
var QpaperSchema = mongoose.Schema({
	
	category: {
		type: String
	},
  title: {
		type: String
  },
  class: {
		type: String
  },
  subject: {
    type: String
  },
	file: {
		type: String
	},
  created_on: {
		type: String
  }
});

const Qpaper = SchemaModel(QpaperSchema, tableName);

module.exports = Qpaper;