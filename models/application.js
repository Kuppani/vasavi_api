const mongoose = require('mongoose');

// Application Schema
//const tableName="Application";
const ApplicationSchema = mongoose.Schema({

	
	Apply_for: {
		type:String
	},
	Applicant_name: {
		type: String
	},
	Qualification: {
		type: String
	},
	Subject: {
		type: String
	},
	DOB: {
		type: String
	},
	Mobile_no: {
		type: Number
	},
	Currently_working_since: {
		type: String
	},
	Currently_working_from: {
		type: String
	},
	Previously_worked_from: {
		type: String
	},
	Previously_worked_to: {
		type: String
	},
	Total_experience: {
		type: Number
	},
	Gender: {
		type: String
	},
	Cv: {
		type: String
	},
	created_on:{
		type: String
	}
});

var Application = module.exports = mongoose.model('Application', ApplicationSchema);


module.exports.saveApplication = function(postApplication, callback){
	postApplication.save(callback);
}

module.exports.getApplication = function(category, callback){
	var query = {category: category};
	Application.find(query, callback);
}

module.exports.getUserById = function(id, callback){
	Application.findById(id, callback);
}

module.exports.remove = function(id, callback){
	Application.findByIdAndRemove({'_id' : id}, function (err,res){
		if(err) throw err;
		callback(null, res);
	  });
}


module.exports.updateApplication = function (id, res, callback) {   

	Application.findByIdAndUpdate(id,{$set:res}, function(err, result){
	if(err) throw err;
	callback(null, result);
	});
}
