const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// News Schema
const tableName="News";
const NewsSchema = mongoose.Schema({
	
title: {
	type: String
},
description: {
	type: String
},
file: {
	  type: String
},
created_on: {
		type: String
}
});

const News = SchemaModel(NewsSchema, tableName);

module.exports = News;
