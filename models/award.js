const SchemaModel = require('../config/schema');
var mongoose = require('mongoose');

// User Schema
const tableName="Award";
var AwardSchema = mongoose.Schema({
		category: {
			type: String
		},
		title: {
			type: String
		},
		received_by: {
			type: String
		},
		given_by: {
			type: String
		},
		year: {
			type: Number
		},
		description: {
			type: String
		},
		url: {
			type: String
		},
		award_image: {
			type: String
		},
		created_on: {
				type: String
		}
});

const Award = SchemaModel(AwardSchema, tableName);

module.exports = Award;
