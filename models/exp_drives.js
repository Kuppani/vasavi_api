const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// ExpDrives Schema
const tableName="ExpDrives";
var ExpDrivesSchema = mongoose.Schema({
	
	ExpDrives_image: {
		type: Array
	},
	category: {
		type: String
    },
	created_on: {
			type: String
	},
	title: {
		type: String
	},
	status: {
			type: String
	}
});

const ExpDrives = SchemaModel(ExpDrivesSchema, tableName);

module.exports = ExpDrives;