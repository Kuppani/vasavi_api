const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// User Schema
const tableName="User";
const UserSchema = mongoose.Schema({
	username: {
		type: String,
		index:true
	},
	password: {
		type: String
	},
	email: {
		type: String
	},
	name: {
		type: String
	}
});

const User = SchemaModel(UserSchema, tableName);

module.exports = User;