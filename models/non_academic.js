const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// NonAcademic Schema
const tableName="Nonacademic";
var NonAcademicSchema = mongoose.Schema({

	category: {
		type: String
	},
	date: {
		type:String
	},
	achievers: {
		type:String
	},
	achievement: {
		type: String
	},
	student: {
		type: Object
	},
	description: {
		type: String
	},
	file: {
		type: String
	},
	created_on: {
			type: String
	}
});

const NonAcademic = SchemaModel(NonAcademicSchema, tableName);

module.exports = NonAcademic;