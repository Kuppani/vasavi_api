const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Calendar Schema
const tableName="Calendar";
const CalenderSchema = mongoose.Schema({
	
	title: {
		type: String
	},
	file: {
		type: String
	},
	created_on: {
			type: String
	}
});

const Calendar = SchemaModel(CalendarSchema, tableName);

module.exports = Calendar;