const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// parliment_committee Schema
const tableName="parliment_committee";
const parliment_committeeSchema = mongoose.Schema({
cabinet_year: {
	type: String
},
cabinet_by: {
	  type: String
},
members: {
		type: String
},
created_on: {
	type: String
}
});

const parliment_committee = SchemaModel(parliment_committeeSchema, tableName);

module.exports = parliment_committee;
