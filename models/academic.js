const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Academic Schema
const tableName="Academic";
const AcademicSchema = mongoose.Schema({

category: {
	type: String
},
date: {
	type:String
},
achievement: {
	type: String
},
student: {
	type: Object
},
description: {
	type: String
},
file: {
	  type: String
},
created_on: {
		type: String
}
});

const Academic = SchemaModel(AcademicSchema, tableName);

module.exports = Academic;
