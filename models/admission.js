const mongoose = require('mongoose');

// Admission Schema
//const tableName="Admission";
const AdmissionSchema = mongoose.Schema({

	Academic_year: {
		type: String
	},
	Admission_into: {
		type:String
	},
	Student_name: {
		type: String
	},
	Aadhar_no: {
		type: String
	},
	Previous_school: {
		type: String
	},
	Mother_name: {
		type: String
	},
	Father_name: {
		type: String
	},
	Email: {
		type: String
	},
	Mobile_no: {
		type: String
	},
	Mother_profession: {
		type: String
	},
	Father_profession: {
		type: String
	},
	Country: {
		type: String
	},
	State: {
		type: String
	},
	City: {
		type: String
	},
	Pincode: {
		type: Number
	},
	Address1: {
		type: String
	},
	Address2: {
		type: String
	},
	created_on: {
		type: String
	}
});

var Admission = module.exports = mongoose.model('Admission', AdmissionSchema);


module.exports.saveAdmission = function(postAdmission, callback){
	postAdmission.save(callback);
}

module.exports.getAdmission = function(category, callback){
	var query = {category: category};
	Admission.find(query, callback);
}

module.exports.getUserById = function(id, callback){
	Admission.findById(id, callback);
}

module.exports.remove = function(id, callback){
	Admission.findByIdAndRemove({'_id' : id}, function (err,res){
		if(err) throw err;
		callback(null, res);
	  });
}


module.exports.updateAdmission = function (id, res, callback) {   

	Admission.findByIdAndUpdate(id,{$set:res}, function(err, result){
	if(err) throw err;
	callback(null, result);
	});
}
