const SchemaModel = require('../config/schema');
const mongoose = require('mongoose');

// Magazine Schema
const tableName="Magazine";
var MagazineSchema = mongoose.Schema({
	
title: {
	type: String
},
file: {
	  type: String
},
created_on: {
		type: String
}
});

const Magazine = SchemaModel(MagazineSchema, tableName);

module.exports = Magazine;
